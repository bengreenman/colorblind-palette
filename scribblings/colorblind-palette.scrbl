#lang scribble/manual

@require[
  scribble/example
  colorblind-palette
  (for-label
    pict
    racket/base
    racket/draw
    racket/contract
    racket/class)]

@(define (url str) (hyperlink str (tt str)))

@title{Colorblind Palette}

@defmodule[colorblind-palette]{
  Lists of colorblind friendly colors.
}

Inspiration: @url{https://davidmathlogic.com/colorblind}

@examples[
  (require pict pict-abbrevs colorblind-palette)
  (ptable
    #:ncols 2
    (for/list ((color* (in-list (list ibm-palette* wong-palette* tol-palette*)))
               (lbl (in-list '(ibm wong tol))))
      (list
        (text (symbol->string lbl) (cons 'bold 'roman) 14)
        (apply ht-append 2
               (for/list (((color idx) (in-indexed color*)))
                 (vc-append 2
                            (filled-rectangle 50 50 #:color color)
                            (text (number->string idx) 'modern 14)))))))
]

@deftogether[(
@defthing[ibm-palette* (listof (is-a?/c color%))]
@defthing[wong-palette* (listof (is-a?/c color%))]
@defthing[tol-palette* (listof (is-a?/c color%))]
)]

