colorblind-palette
===

Color palettes designed for the colorblind.

From: <https://davidmathlogic.com/colorblind>


#### Install

Either from the package catalog (`--clone` puts a copy in the current directory):

```
raco pkg install --clone colorblind-palette
```

Or from source:

```
git clone git@gitlab.com:bengreenman/colorblind-palette
cd colorblind-palette
raco pkg install
```


#### Example Use

```
#lang racket/base
(require colorblind-palette pict)
(disk 50 #:color (wong-palette 3))
```


#### License

Blue Oak Model License


